$("#allegro").on("change", function () {
    $('#status_charger').html("");
    $('#status_exporter').html("");
    if ($('#allegro')[0].files[0]) {
      var xmlname = $('#allegro')[0].files[0].name;
      var xmltype = $('#allegro')[0].files[0].type;
      if (xmltype == "text/xml") {
        $('#status_charger').html("<p class='succes'>Le fichier "+xmlname+" a bien été chargé. Vous pouvez le convertir.</p>");
      } else {
        $('#status_charger').html("<p class='erreur'>Le fichier "+xmlname+" ne semble pas être au format XML. Vous ne pourrez pas le convertir.</p>");
      }
    }
  });

  // EXPORT TXT
  $("#txt").on("click", function () {
    $('#status_charger').html("");
    $('#status_exporter').html("");
    // Fichier dans le buffer
    var allegro = $('#allegro')[0].files[0];

    // Traitement du fichier
    if (allegro) {
      // Nom du fichier
      var n1 = allegro.name;
      var n2 = n1.replace('.xml', '');
      var txtname = n2 + ".txt";
      $('#status_exporter').html("Préparation du fichier <img src='ajax-loader.gif'>");

      // Lecture du fichier
      var reader = new FileReader();

      reader.readAsText(allegro, "UTF-8");
      reader.onload = function (e) {
        var contentxml = e.target.result;
        // Vérification du format Allegro
        var regex = /http:\/\/www\.atilf\.fr\/allegro/gi;
        if (regex.test(contentxml)) {
        // Réencodage utf-8
        contentxml = utf8encode(contentxml);
        // Remplacer les entitées HTML
        contentxml = decodeHtml(contentxml);

        // replace text
        var contentxml = contentxml
          // Supprimer les balises de début
          .replace(/<\?xml(.|\n)*?<text>/gi, "")
          // Supprimer les balises de fin
          .replace(/<\/text>(.|\n)*?<\/TEI>/gi, "")
          // Formater les formes graphiques
          .replace(/<x:wf word='([^']*)' pos='[^']*' lemma='[^']*'\/>/gi, "$1")
          // Formater les étoiles
          .replace(/<c type='etoile'\/>/gi, "*")
          // Formater les numéros de pages
          .replace(/<pb n='(\d+)'\/>/gi, "\n\np.$1\n\n")
          // Formater les titres
          .replace(/<head>((.|\n)*?)<\/head>/gi, "$1\n")
          // Formater les fins de lignes
          .replace(/(<lb\/>|<\/[pl]>)/gi, "\n")
          // Supprimer toutes les balises
          .replace(/<[^>]*?>/gi, "")
          // Supprimer les espaces multiples
          .replace(/ +/gi, " ")
          ;

        download(contentxml, txtname, "text/plain");
        // console.log(contentxml);

        $('#status_exporter').html("<p class='succes'>Le fichier " + txtname + " a été téléchargé !</p>");
        } else {
          $('#status_exporter').html("<p class='erreur'>Erreur : le fichier " + n1 + " n'a pas été téléchargé depuis la base Frantext !</p>");
        }
      }
      reader.onerror = function (e) {
        $('#status_exporter').html("<p class='erreur'>Erreur de lecture du fichier " + txtname + " !</p>");
      }
    } else {
      // Pas de fichier
      $('#status_charger').html("<p class='erreur'>Aucun fichier n'a été chargé ! Utilisez le bouton ci-dessus !</p>");
    }
  });
  // Export XML-TEI
  $("#xml").on("click", function () {
    $('#status_charger').html("");
    $('#status_exporter').html("");
    // Fichier dans le buffer
    var allegro = $('#allegro')[0].files[0];

    // Traitement du fichier
    if (allegro) {
      // Nom du fichier
      var n1 = allegro.name;
      var n2 = n1.replace('.xml', '');
      var txtname = n2 + ".tei.xml";
      $('#status_exporter').html("Préparation du fichier <img src='ajax-loader.gif'>");

      // Lecture du fichier
      var reader = new FileReader();

      reader.readAsText(allegro, "UTF-8");
      reader.onload = function (e) {
        var contentxml = e.target.result;
        // Vérification du format Allegro
        var regex = /http:\/\/www\.atilf\.fr\/allegro/gi;
        if (regex.test(contentxml)) {
        // Réencodage utf-8
        contentxml = utf8encode(contentxml);
        // replace text
        // console.log(contentxml);
        var contentxml = contentxml
          // Ajouter schéma Frantext
          .replace(/(<\?xml version=\"1.0\" encoding=\"UTF-8\" \?>)/gi, "$1\n<?oxygen RNGSchema=\"https://apps.atilf.fr/schemas/frantext/frantext.rnc\" type=\"compact\"?>\n")
          // Ajouter IDNO
          .replace(/(<idno type='FRANTEXT'>)(<\/idno>)/gi, "$1"+n2+"$2")
          // Supprimer références status
          .replace(/(<availability status='([^"']*)'> <p> <\/p> <\/availability>)/gi, "")
          // Formater les formes graphiques
          .replace(/<x:wf word='([^']*)' pos='([^']*)' lemma='([^']*)'\/>/gi, "$1")
          // Formater les étoiles
          .replace(/<c type='etoile'\/>/gi, "*")
          // Formater les fins de lignes
          .replace(/(<lb\/>|<\/[pl]>|<\/teiHeader>|<\/?body>|<\/?text>|<\/div>|<\/head>)/gi, "$1\n")
          .replace(/(<head>)/gi, "\n$1")
          .replace(/(<pb n='\d+'\/>)/gi, "\n$1\n")
          // Supprimer les espaces multiples
          .replace(/ +/gi, " ")
          // Corriger les guillemets
          .replace(/\'/gi, "\"")
          ;
        download(contentxml, txtname, "text/xml");
        // console.log(contentxml);

        $('#status_exporter').html("<p class='succes'>Le fichier " + txtname + " a été téléchargé !</p>");
        } else {
          $('#status_exporter').html("<p class='erreur'>Erreur : le fichier " + n1 + " n'a pas été téléchargé depuis la base Frantext !</p>");
        }
      }
      reader.onerror = function (e) {
        $('#status_exporter').html("<p class='erreur'>Erreur de lecture du fichier " + txtname + " !</p>");
      }
    } else {
      // Pas de fichier
      $('#status_charger').html("<p class='erreur'>Aucun fichier n'a été chargé ! Utilisez le bouton ci-dessus !</p>");
    }
  });

  // EXPORT XML-TEI ANNOTE
  $("#ann").on("click", function () {
    $('#status_charger').html("");
    $('#status_exporter').html("");
    // Fichier dans le buffer
    var allegro = $('#allegro')[0].files[0];

    // Traitement du fichier
    if (allegro) {
      // Nom du fichier
      var n1 = allegro.name;
      var n2 = n1.replace('.xml', '');
      var txtname = n2 + ".annote.xml";
      $('#status_exporter').html("Préparation du fichier <img src='ajax-loader.gif'>");

      // Lecture du fichier
      var reader = new FileReader();

      reader.readAsText(allegro, "UTF-8");
      reader.onload = function (e) {
        var contentxml = e.target.result;
        // Vérification du format Allegro
        var regex = /http:\/\/www\.atilf\.fr\/allegro/gi;
        if (regex.test(contentxml)) {
        // Réencodage utf-8
        contentxml = utf8encode(contentxml);

        // replace text
        var contentxml = contentxml
          // Ajouter IDNO
          .replace(/(<idno type='FRANTEXT'>)(<\/idno>)/gi, "")
          // Supprimer références status
          .replace(/(<availability status='([^"']*)'> <p> <\/p> <\/availability>)/gi, "")
          // Formater les formes graphiques
          .replace(/<x:wf word='([^']*)' pos='([^']*)' lemma='([^']*)'\/>/gi, "<w ana=\"\#$2\" lemma=\"$3\">$1</w>")
          // Formater les étoiles
          .replace(/<c type='etoile'\/>/gi, "*")
          // Formater les fins de lignes
          .replace(/(<lb\/>|<\/[pl]>|<\/teiHeader>|<\/?body>|<\/?text>|<\/div>|<\/head>)/gi, "$1\n")
          .replace(/(<head>)/gi, "\n$1")
          .replace(/(<pb n='\d+'\/>)/gi, "\n$1\n")
          // Supprimer les espaces multiples
          .replace(/ +/gi, " ")
          // Corriger les guillemets
          .replace(/\'/gi, "\"")
          ;
        download(contentxml, txtname, "text/xml");
        // console.log(contentxml);

        $('#status_exporter').html("<p class='succes'>Le fichier " + txtname + " a été téléchargé !</p>");
      } else {
          $('#status_exporter').html("<p class='erreur'>Erreur : le fichier " + n1 + " n'a pas été téléchargé depuis la base Frantext !</p>");
        }
      }
      reader.onerror = function (e) {
        $('#status_exporter').html("<p class='erreur'>Erreur de lecture du fichier " + txtname + " !</p>");
      }
    } else {
      // Pas de fichier
      $('#status_charger').html("<p class='erreur'>Aucun fichier n'a été chargé ! Utilisez le bouton ci-dessus !</p>");
    }
  });

  // Décoder les entitées HTML
  // https://stackoverflow.com/questions/5796718/html-entity-decode
  function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }

  // Encoder en UTF-8
  // https://openclassrooms.com/forum/sujet/la-fonction-quot-utf8decodequot-en-js-sa-existe-93843
  function utf8encode(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }
    return utftext;
  }
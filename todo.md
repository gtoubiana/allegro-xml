# TODO

# MAYDO
- Intégration dans frantext-evolution
- Intégration dans un template Bootstrap (npm)
- Minifier le js et css
- Utiliser xsl pour la conversion ? 
    - https://www.youtube.com/watch?v=L8NM_5U3Pos
    - https://developer.mozilla.org/fr/docs/Web/XSLT/Interface_XSLT_JS_dans_Gecko/Exemple_basique
    - https://developer.mozilla.org/fr/docs/Web/XSLT/Interface_XSLT_JS_dans_Gecko/Les_liaisons_JavaScript_XSLT*

# DONE
- 2021-05-26
<!-- - corriger le chargement de fichiers xml (message d'erreur ?) -->
- 2021-03-12
<!-- - fichiers style.css et script.js -->
- 2021-03-10
<!-- - Mise en garde si il ne s'agit pas d'un fichier allegro -->
- 2021-03-05
<!-- - Supprimer les références de status dans le header (tei) (revoir \n et " + ') -->
<!-- - Supprimer les références de status dans le header (ann) -->
<!-- - Transformer en site web => https://gtoubiana.frama.io/allegro-xml/ -->
- 2021-03-03
<!-- * Export en .tei.xml -->
<!-- * Export en .annote.xml -->
<!-- * corriger les apostrophes ' en " -->
- 2021-02-05
<!-- * Résolution des problèmes de codage UTF-8 -->
<!-- * Export en .txt -->
- 2021-02-03
<!-- * Première version -->
